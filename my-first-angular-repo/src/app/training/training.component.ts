import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { TrainingService } from './service/training.service';

@Component({
  selector: 'app-training',
  templateUrl: './training.component.html',
  styleUrls: ['./training.component.css']
})
export class TrainingComponent implements OnInit {
  ongoingtraining=false;
  exerciseSubscription:Subscription;

  constructor(private trainingService: TrainingService) { }


  ngOnInit(): void {

    this.trainingService.exerciseChanged.subscribe

    this.exerciseSubscription = this.trainingService.exerciseChanged.subscribe(ex => {
      if (ex) {
        this.ongoingtraining = true;
      } else {
        this.ongoingtraining = false;
      }
      
    })

  }

}
