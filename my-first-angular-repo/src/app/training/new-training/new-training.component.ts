import { Component, OnInit, OnDestroy} from '@angular/core';
import {map} from 'rxjs/operators/map'
import { TrainingService } from '../service/training.service';
import { Exercise } from '../../core/model/exercise.model';
import { NgForm } from '@angular/forms';
import {  AngularFirestore } from '@angular/fire/firestore';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'app-new-training',
  templateUrl: './new-training.component.html',
  styleUrls: ['./new-training.component.css']
})
export class NewTrainingComponent implements OnInit,OnDestroy {
  

  exercisesChangedSubscription:Subscription
   exercises: Exercise[];

  constructor(private trainingService: TrainingService) { }

  ngOnInit(): void {
    this.exercisesChangedSubscription=  this.trainingService.exercisesChanged.
                                        subscribe(exercises => (this.exercises = exercises) );
    this.trainingService.fetchAvailableExercise();
//    this.exercises = this.trainingService.getAvailableExercises();

  }

  ngOnDestroy(): void {
    this.exercisesChangedSubscription.unsubscribe();
   }



  onStartTraining(form:NgForm) {

    this.trainingService.startExercise(form.value.exercise);

  }

}
