import { Exercise } from '../../core/model/exercise.model';
import { Subject, Subscription } from 'rxjs';
import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';

@Injectable()
export class TrainingService {

exerciseChanged = new Subject<Exercise>();
    private availableExercise: Exercise[] = [];
    
    exercisesChanged = new Subject<Exercise[]>();
    finishExerciseChanged = new Subject<Exercise[]>();

    private runningExercise: Exercise;
    private finishedExercises: Exercise[] =[]
    private fbSubs: Subscription[] = []


    public constructor(private db: AngularFirestore) {

    }

    public fetchAvailableExercise() {
        this.fbSubs.push(this.db.collection('availableExercies').
        snapshotChanges().
        pipe(map(docArray => {
           return  docArray.map(doc => {
             const exercise = doc.payload.doc.data() as Exercise;
              return <Exercise>{
              id: doc.payload.doc.id,
              name: exercise.name,
              duration: exercise.duration,
              data: exercise.data,
              calories: exercise.calories
              //calories: doc.payload.doc.calories,
    
    
            };
          });
        })
        ).subscribe((exercises: Exercise[]) => {
            this.availableExercise = exercises;
            this.exercisesChanged.next([...this.availableExercise]);
        }));
    }

    startExercise(selectedId: string) {
        console.log("Training Service: " + selectedId);
        const selectedExercise = this.availableExercise.find(ex => ex.id === selectedId)
        this.runningExercise = selectedExercise;
        this.exerciseChanged.next({...this.runningExercise});
    }

     getRunningExercise() {
        return  { ...this.runningExercise}
    }

    completeExercise() {
        this.addDataToDataBase({
            ...this.runningExercise,
            data: new Date(),
            state: 'cancelled'
        });
        this.runningExercise = null;
        this.exerciseChanged.next(null);
    }

    cancelExercise(progress: number) {

        this.addDataToDataBase({
            ...this.runningExercise,
            duration:this.runningExercise.duration * (progress/100),
            calories: this.runningExercise.calories * (progress/100),
            data: new Date(),
            state: 'completed'
        });
        this.runningExercise = null;
        this.exerciseChanged.next(null);



    }

fetchCompletedOrCancelleeExercises() {
    this.fbSubs.push(this.db.collection('finishedExercises').
    valueChanges().
    subscribe((exercises: Exercise[]) => {
        this.finishExerciseChanged.next(exercises);
    }, error => {
        console.log(error);
    }));
    }

    private addDataToDataBase(exercise: Exercise) {
        this.db.collection('finishedExercises').add(exercise);

    }

    cancelSubscriptions() {

        this.fbSubs.forEach(sub => sub.unsubscribe());
    }

}
