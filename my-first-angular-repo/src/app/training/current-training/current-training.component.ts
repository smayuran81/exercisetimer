import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { StopTrainingComponent } from './stop-training.component';
import { TrainingService } from '../service/training.service';


@Component({
  selector: 'app-current-training',
  templateUrl: './current-training.component.html',
  styleUrls: ['./current-training.component.css']
})
export class CurrentTrainingComponent implements OnInit {
  progress =0;
  timer: number;
  @Output() trainingExit = new EventEmitter();

  constructor(private dialog: MatDialog, private trainingService: TrainingService) { }

  ngOnInit(): void {
    this,this.startOrResumeTimer();
  }

  startOrResumeTimer() {
    const step = this.trainingService.getRunningExercise().duration / 100 * 1000;
    console.log(this.trainingService.getRunningExercise().duration);
    console.log("step: "+ step);

    this.timer = setInterval(() => {

      this.progress = this.progress + 5 ;
      console.log(this.progress)
      if(this.progress >= 100) {
        this.trainingService.completeExercise();
        clearInterval(this.timer);
      }
    }, step);

  }

  onStop(): void {
    clearInterval(this.timer);
   const dialogRef = this.dialog.open(StopTrainingComponent, {data: {progress: this.progress}});
   dialogRef.afterClosed().subscribe(result=> {
     console.log(result)
     if(result) {
       this.trainingExit.emit();
       this.trainingService.cancelExercise(this.progress);
     } else {
       this.startOrResumeTimer();
     }
   })
  }

}
