import { Component } from "@angular/core";

@Component({
  selector:' app-products',
  templateUrl:'./product.component.html'
})
export class ProductsComponent{
  products = ['A Book', 'A Tree'];
  isDisabled: boolean = true;
productName = ' A Book';
  constructor(){
    setTimeout(() => {
     // this.productName = ' A Tree';
      this.isDisabled=false;
    }, 3000);
    
  }

  addProduct(): void {

    this.products.push(this.productName);
  }
}
