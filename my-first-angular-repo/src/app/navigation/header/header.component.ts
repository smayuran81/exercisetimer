import { Component, OnInit, Output, EventEmitter, OnDestroy } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { Subscription } from 'rxjs/Subscription';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy{

  isAuth: boolean = false;
  authSubscription: Subscription;

 
  @Output() sidenavToggle = new EventEmitter<void>();

  constructor(private authService: AuthService) { }

  ngOnInit(): void {

    this.authSubscription = this.authService.authChange.subscribe(authStatus=> {
      this.isAuth = authStatus;
    })
  }

  ngOnDestroy(): void {
    this.authSubscription.unsubscribe();
   }


  onToggleSideNav() {

    this.sidenavToggle.emit();

  }

  onLogout() {
    this.authService.logout();
    
  }


}
