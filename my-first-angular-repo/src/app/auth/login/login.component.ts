import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';
import { UIService } from '../../shared/ui.service';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {
  loginForm: FormGroup;
  isLoading: boolean = false;
  private loadingSubs: Subscription;

  constructor(private authService: AuthService, private uiService: UIService) { }

  ngOnInit(): void {
    this.loadingSubs = this.uiService.loadingStateChanged.subscribe( isLoadingValue => {
      this.isLoading = isLoadingValue;
    })
    this.loginForm = new FormGroup({
      email: new FormControl('' ,{
        validators: [Validators.required, Validators.email]
      }),
      password: new FormControl('', {validators: [Validators.required]
      }),
    })
   
  }

  isDisabled()   {
    this.loginForm.statusChanges.subscribe(status => {
      if(status){
        return true
      }else {
        return false;
      }
    })
   
  }

  form: FormGroup = new FormGroup({
    username: new FormControl(''),
    password: new FormControl(''),
  });

  submit() {
    if (this.form.valid) {
      this.submitEM.emit(this.form.value);
    }
    this.authService.login({
      email: this.form.value.username,
      password: this.form.value.password

    })
  }
  @Input() error: string | null;

  @Output() submitEM = new EventEmitter();

  onSubmit() {
    this.authService.login({
      email: this.loginForm.value.email,
      password: this.loginForm.value.password
    });
  }

  ngOnDestroy() {
    this.loadingSubs.unsubscribe();
  }

}
