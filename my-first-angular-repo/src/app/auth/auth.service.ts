import { User } from './user.model';
import { AuthData } from './auth-data.model';
import { Subject } from 'rxjs/Subject'
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { TrainingService } from '../training/service/training.service';
import { MatSnackBarModule, MatSnackBar } from '@angular/material/snack-bar';
import { UIService } from '../shared/ui.service';

@Injectable()
export class AuthService {
    private isAuthenticated = false;
    authChange = new Subject<boolean>();

    constructor(private router: Router, private afAuth: AngularFireAuth, 
        private trainingService: TrainingService,
        
        private uiService: UIService) {

    }

    registerUser(authData: AuthData) {
        this.uiService.loadingStateChanged.next(true);
        this.afAuth.auth.
            createUserWithEmailAndPassword(authData.email, authData.password).
            then(result => {
                this.uiService.loadingStateChanged.next(true);
                console.log(result)

            }).
            catch(error => {
                console.log(error);
                this.uiService.loadingStateChanged.next(true);
                this.uiService.showSnackBar(error.message, null, {duration: 2000});
                
            });

        // this.user ={
        //     email: authData.email,
        //     userId: Math.round(Math.random()* 1000).toString()
        // }
        // ;  
    }

    initAuthListeneer() {
        this.afAuth.authState.subscribe(user => {
            if (user) {
                this.isAuthenticated = true;
                this.authChange.next(true);
                this.router.navigate(['/Training']);
            } else {
                this.trainingService.cancelSubscriptions();
                this.isAuthenticated = false;
                this.authChange.next(false);
                this.router.navigate(['/Login'])

            }
        });
    }

    logout() {

        this.afAuth.auth.signOut();

    }

    /*    getUser(): User {oa
           return { ...this.user};
       } */

    isAUth(): boolean {
        return this.isAuthenticated;
    }

    login(authData: AuthData): void {
        this.uiService.loadingStateChanged.next(true);
        this.afAuth.auth.signInWithEmailAndPassword(
            authData.email,
            authData.password
        ).then(result => {
            console.log(result)
            this.uiService.loadingStateChanged.next(true);

        }).
            catch(error => {
                this.uiService.loadingStateChanged.next(false);
                console.log(error);
                this.uiService.showSnackBar(error.message, null, {duration: 2000});
            });
    }

}