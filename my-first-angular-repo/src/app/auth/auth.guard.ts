import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { AuthService } from '../auth/auth.service';

@Injectable()
export class AuthGuard implements CanActivate {
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
   
        if(this.authService.isAUth()) {
            return true;
        }
        else [
            this.router.navigate(['/Login'])
        ]
       
    }

 
    constructor(
        private authService: AuthService, private router: Router){

    }



  

}