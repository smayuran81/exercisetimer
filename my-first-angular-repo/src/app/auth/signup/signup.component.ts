import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from '../auth.service';
import { from, Subscription } from 'rxjs';
import { UIService } from '../../shared/ui.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit, OnDestroy {
  isLoading: boolean = false;
  private loadSub: Subscription;

  maxDate;

  constructor(private authService: AuthService, private uiService: UIService) { }

  ngOnInit(): void {

    this.loadSub = this.uiService.loadingStateChanged.subscribe(loadStateValue => {
      this.isLoading = loadStateValue;
      console.log(this.isLoading);
    })
    this.maxDate = new Date();
    this.maxDate.setDate(this.maxDate.getFullYear()-18);
  }

  onSubmit(form: NgForm){
    console.log(form);
    this.authService.registerUser({
      email: form.value.email,
      password: form.value.password

    });
  }

  ngOnDestroy() {
    this.loadSub.unsubscribe();
  }

}
