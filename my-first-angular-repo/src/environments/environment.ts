// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyD1rrEL22sVjJTN5EuT-t49B8KwFesNjno",
    authDomain: "ng-fitness-tracker-9eeb6.firebaseapp.com",
    databaseURL: "https://ng-fitness-tracker-9eeb6.firebaseio.com",
    projectId: "ng-fitness-tracker-9eeb6",
    storageBucket: "ng-fitness-tracker-9eeb6.appspot.com",
    messagingSenderId: "183464828169",
    appId: "1:183464828169:web:a1ec3884c5305f5d6c47d8",
    measurementId: "G-CRMS058G68"
  }
};